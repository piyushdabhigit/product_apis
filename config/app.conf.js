import logger from "morgan";
import helmet from "helmet";
import express from "express";
import cookieParser from "cookie-parser";
import cors from "cors";

export default class ApplicationConfig {
  static init(app) {
     
    //don't show the log when it is test
    if (process.env.NODE_ENV !== "test") {
      app.use(logger("dev"));
    }
    app.use(helmet());
    app.use(express.json());
    app.use(
      express.urlencoded({
        extended: false,
      })
    );
    app.use(cookieParser());
     
    //To allow cross-origin requests
    app.use(cors());
  }
}

