/**
 * @description Express Framework module
 * @param express
 */
import express from "express";

/**
 * @description User Controller
 * @param ProductController
 */

import ProductController from "../controller/ProductController.js";
import AuthMiddleware from "../../../middlewares/auth.js";

/**
 * @description Express Framework Router
 * @param Router
 */
let ProductRouter = express.Router();
ProductRouter.post("/addProduct",AuthMiddleware.auth,ProductController.addProduct);  
ProductRouter.get("/getProduct",AuthMiddleware.auth,ProductController.getProduct);  
ProductRouter.put("/updateProduct/:id",AuthMiddleware.auth,ProductController.updateProduct);  
ProductRouter.delete("/deleteProduct/:id",AuthMiddleware.auth,ProductController.deleteProduct);  
ProductRouter.get("/getProductById/:id",AuthMiddleware.auth,ProductController.getProductById);  


/**
 * @description Configured router for User Routes
 * @exports ProductRouter
 * @default
 */
export default ProductRouter;
