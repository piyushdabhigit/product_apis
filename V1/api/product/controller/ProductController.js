

import mongoose from "mongoose";
import ProductModel from "../../../../models/ProductModel.js";
import validator from "express-validator";
const { body, header, validationResult } = validator;

//const validator = require("validator");
/**
 * @description API Response Utility functions
 * @param {Function} successResponse - Success Response with message
 * @param successResponseWithData - Success Response with data
 * @param errorResponse - Error Response with message
 * @param errorResponseWithData - Error Response with data
 * @param validationErrorWithData - Validation Error with data
 * @param validationError - Validation Error with message
 * @param unauthorizedResponse - Unauthorized Error response handling
 * @param unprocessable - Unprocessable Error response handling
 */
import {
    successResponse,
    successResponseWithData,
    errorResponse,
    errorResponseWithData,
    notFoundResponse,
    validationErrorWithData,
    validationError,
    unauthorizedResponse,
    unprocessable,
} from "../../utils/apiResponse.js";

/**
 * @description User Constants
 * @param ProductConstants
 */
import { ProductConstants } from "../const.js";
import { middlewaresConstants } from "../../../middlewares/const.js";

const getProduct = [

    async (req, res) => {

        try {
            //const results = await ProductModel.find({}, { __v: 0 });
            //res.send(results);

            console.log('req.header("page")', req.header("page"))
            console.log('req.header("limit")', req.header("limit"))
            var sortQuery = { name: 1 };
            if (req.header("limit") != "" && req.header("limit") != undefined && req.header("page") != "" && req.header("page") != undefined) {
                const results = await ProductModel.find({}).sort(sortQuery).limit(req.header("page") * 1).skip((req.header("page") - 1) * req.header("limit"));

                return successResponseWithData(
                    res, ProductConstants.productFeatched,
                    ({ total: await ProductModel.find({}).count(), results })
                );
            }
            else {

                const results = await ProductModel.find({});
                return successResponseWithData(
                    res, ProductConstants.productFeatched,
                    ({ total: await ProductModel.find({}).count(), results })
                );

            }


        }
        catch (e) {
            return errorResponse(res, e);
        }



    }]

const getProductById = [

    async (req, res) => {
        const id = req.params.id;
        try {

            const results = await ProductModel.find({ _id: mongoose.Types.ObjectId(id) });
            return successResponseWithData(
                res, ProductConstants.productFeatched,
                ({ total: await ProductModel.find({}).count(), results })
            );

        }
        catch (e) {
            return errorResponse(res, e);
        }
    }]

const addProduct = [
    // Validate and Sanitize fields.   
    body("name")
        .isString()
        .trim()
        .escape()
        .withMessage(ProductConstants.nameRequired),
    body("category")
        .isString()
        .trim()
        .escape()
        .withMessage(ProductConstants.categoryRequired),
    body("price")
        .isString()
        .trim()
        .escape()
        .withMessage(ProductConstants.priceRequired),
    async (req, res) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            // Display sanitized values/errors messages.
            return validationErrorWithData(
                res,
                ProductConstants.validationError,
                errors.array()
            );
        }
        else {

            try {
                const product = new ProductModel(req.body);
                console.log('aaa', product)
                const result = await product.save();
                console.log('bbb', result)
                return successResponseWithData(res, ProductConstants.contactSaved, result)

            }
            catch (e) {
                return errorResponse(res, e);
            }
        }
    }]

const updateProduct = [

    async (req, res) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            // Display sanitized values/errors messages.
            return validationErrorWithData(
                res,
                ReelConstants.validationError,
                errors.array()
            );
        }
        else {


            try {
                const id = req.params.id;
                const updates = req.body;
                const options = { new: true };

                const result = await ProductModel.findByIdAndUpdate(id, updates, options);
                if (!result) {

                    return errorResponse(res, "Product does not exist");

                }
                return successResponseWithData(res, "Contact Updated Successfully", result)
                //res.send(result);
            }
            catch (err) {
                return errorResponse(res, ProductConstants.err);
            }


        }


    }]

const deleteProduct = [

    async (req, res) => {

        const id = req.params.id;
        try {
            const result = await ProductModel.findByIdAndDelete(mongoose.Types.ObjectId(id));

            if (!result) {
                return errorResponse(res, "Product does not exist");
            }
            return successResponse(res, ProductConstants.productDeleted);

        } 
        catch (e) {
            return errorResponse(res, e);
        }

    }]

export default {
    addProduct,
    getProduct,
    getProductById,
    updateProduct,
    deleteProduct
};
