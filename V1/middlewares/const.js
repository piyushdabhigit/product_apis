const middlewaresConstants = {
   
  errorOccurred: "Error Occured.", 
  successMsg: "Success.",
  authError:"Please authenticate.",
  notEnoughBalance:"You account does not have sufficient balance.",
  licenseExpired:"Your licence has expired.",
  //invalidAPIKeyAndLicense: "Either your api_key is invalid or your license is expired.",
  invalidAPIKeyAndLicense: "Your license is expired.",
  invalidClientIdMessage: "Invalid client_id. Plese try again.",
  invalidAPIKeyMessage: "Invalid api_key. Plese try again.",
};

export { middlewaresConstants }; 
