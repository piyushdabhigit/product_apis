module.exports = errorHandler;

function errorHandler(err, req, res, next) {
    if (typeof (err) === 'string') {
        // custom application error

        return res.status(404).json({ message: err });

    }

    if (err.name === 'ValidationError') {
        // mongoose validation error
        return res.status(404).json({ message: err.message });
    }

    if (err.name === 'UnauthorizedError') {
        // jwt authentication error
        return res.status(404).json({ message: 'Invalid Token' });
    }

}