import jwt from "jsonwebtoken";
import User from "../../models/UsersModel.js";

/**
 * @description API Response Utility functions
 * @param {Function} successResponse - Success Response with message
 * @param successResponseWithData - Success Response with data
 * @param errorResponse - Error Response with message
 * @param errorResponseWithData - Error Response with data
 * @param validationErrorWithData - Validation Error with data
 * @param validationError - Validation Error with message
 * @param unauthorizedResponse - Unauthorized Error response handling
 * @param unprocessable - Unprocessable Error response handling
 */
 import {
  successResponse,
  successResponseWithData,
  errorResponse,
  errorResponseWithData,
  notFoundResponse,
  validationErrorWithData,
  validationError,
  unauthorizedResponse,
  unprocessable,
} from "../api/utils/apiResponse.js";

const auth = async (req, res, next) => {
  try {
    if (req.header("Authorization")) {
      const authToken = req.header("Authorization").replace("Bearer ", "");
      const data = jwt.verify(authToken, process.env.JWT_SECRET);
       
      const users = await User.findOne({
        _id: data.id,
      });

      if (!users) {
        throw new Error();
      }
      req.users = users;
      req.authToken = authToken;
      next();
    } else {
      throw new Error();
    }
  } catch (error) {
     
    return unauthorizedResponse(res, "Not authorized to access this resource");
  }
};

export default {
  auth
};
