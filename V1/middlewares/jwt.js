import expressJwt from "jsonwebtoken";
import Users from "../../models/usersModel.js";
import { middlewaresConstants } from "../middlewares/const.js";
import validator from "express-validator";
import mongoose from "mongoose";
const { body,header, validationResult } = validator;

const jwt = [

  

  header("api_key")
    .isString()
    .trim()
    .escape()
    .withMessage(middlewaresConstants.apiKeyRequired),
  header("client_id")
    .isString()
    .trim()
    .escape()
    .withMessage(middlewaresConstants.clientIdRequired),

  async (req, res, next) => {

    /**
   * @function errorResponse
   * @description Function that returns response with Error message
   * @param {Object} res - Express Framework Response Object
   * @param {String} msg - Message
   */
    function errorResponse(res, msg) {
      var data = {
        status: 0,
        responseCode: 400,
        message: msg,
      };
      return res.status(200).json(data);
    };
    try {
     // console.log('JWTTTTTTT')
      //console.log('token',req.header("token"),'apikey',req.header("api_key"),'Idddddddddd',req.header("client_id"))
      const token = req.header("token").replace("Bearer ", "");
      //console.log('token',token)
      const decoded = expressJwt.verify(token, req.header("api_key") + req.header("client_id"));
      //console.log('decoded',decoded)

      
      const user = await Users.findOne({
        userId: decoded.userId,
        //"authToken": token 
        "tapiDetails.tapiPartners.authToken": token,
        "tapiDetails.tapiPartners.partnerId": mongoose.Types.ObjectId(req.header("api_key"))
      });
      

      if (!user) {
        throw new Error();
      }



      var datetime = new Date();

      var endDate = user.endDate.toISOString();

      if (endDate < datetime.toISOString()) {

        //res.status(401).send({ error: "Your licence has expired." });
        return errorResponse(res, middlewaresConstants.licenseExpired);


      }
      else {


        // check balance

      
        if (user.tapiDetails.isLicenceAllocated == true && user.tapiDetails.activeBalanceTapi > 0) {
          //   console.log('True')
          req.token = token;
          req.user = user;
          next();
        }
        else if (user.tapiDetails.isLicenceAllocated == false) {
          //console.log('welcome')
         // console.log('EndDate Akanksha')
          //const reseller = await Partners.findOne({ _id: user.resellerId });
          const reseller = await Partners.findOne({ _id: mongoose.Types.ObjectId(req.header("api_key")) });
        
         // console.log('EndDate Akanksha',reseller)
          if (reseller.activeBalanceTapi > 0) {

            req.token = token;
            req.user = user;
            next();
          }
          else {
            return errorResponse(res, middlewaresConstants.notEnoughBalance);
          }


        }
        else {
          return errorResponse(res, middlewaresConstants.notEnoughBalance);
        }


      }
    } catch (e) {
      //console.log(e);
      //res.status(400).send({ error: "Please authenticate." });
       
      return errorResponse(res, middlewaresConstants.authError);
    }
}];

export default jwt;
