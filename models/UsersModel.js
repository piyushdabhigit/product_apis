import mongoose from "mongoose";
const Schema = mongoose.Schema;
import md5 from "md5";
import jwt from "jsonwebtoken";
import validator from "validator";
var UsersSchema = new mongoose.Schema(
{
    
    email: {
      type: String,
      trim: true,
      unique: true,
      lowercase: true,
      dropDups: true,
      validate(value) {
        if (!validator.isEmail(value)) {
          throw new Error("Email is not valid.");
        }
      }
    },
    password: {
      type: String,
      required: [true, "Password field is required."],
      validate(value) {
        if (value.toLowerCase().includes("password")) {
          throw new Error('Password can not contain "password."');
        }
      }
    },
    fullName: { type: String },
    authToken: { type: String, default: "" },
     

  },
  { timestamps: true },
  { collection: 'users' });

UsersSchema.methods.toJSON = function () {
  var obj = this.toObject();
  delete obj.password;
  return obj;
}

UsersSchema.pre("save", function (next) {
  this.updatedAt = new Date();
  next();
});
 
const users = mongoose.models.users || mongoose.model("users", UsersSchema);
export default users;
 