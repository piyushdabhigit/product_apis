import mongoose from "mongoose";
const Schema = mongoose.Schema; 
import validator from "validator";
var ProductSchema = new mongoose.Schema(
  { 
    name: {
      type: String,
      required: true
    },
    category: {
      type: String,
      required: true
    }, 
    remarks: {
      type: String
       
    }, 
    price: {
      type: Number,
      required: true
    },  
  },
  { timestamps: true },
  { collection: 'product' });

ProductSchema.pre("save", function (next) {
  this.updatedAt = new Date();
  next();
});

const product = mongoose.models.product || mongoose.model("product", ProductSchema);
export default product;